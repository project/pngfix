PNG Fix for Drupal
==================

This module includes a fix to correctly handle PNG
transparency in Windows Internet Explorer 5.5 and 6.

Branch
------
Please check out the branch according to your Drupal version...

Drupal 5:
  DRUPAL-5--2
  
Drupal 6:
  DRUPAL-6--1
